import { ProductService } from './product.service';
import { Body, Controller, Post } from '@nestjs/common';
import { Product } from './product.schema';
import { productDto } from './product.dto';

@Controller('products')
export class ProductController {
  constructor(private productService: ProductService) {}

  @Post()
  createProduct(@Body() params: productDto): Promise<Product> {
    return this.productService.create(params);
  }
}

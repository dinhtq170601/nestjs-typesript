import { IsNumber, IsString } from 'class-validator';

export class productDto {
  @IsString({ message: 'Thiếu tên sản phẩm' })
  name: string;

  @IsNumber()
  price: number;

  @IsString({ message: 'Thiếu mô tả sản phẩm' })
  description: string;
}
